package com.github.agnikowa.moneytransferhandler

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import akka.util.Timeout
import com.typesafe.scalalogging.LazyLogging

import scala.concurrent.{ExecutionContext, Future}
import scala.language.postfixOps
import scala.util.{Failure, Success}

object TransferApp extends App with AppConfig with LazyLogging {

  import scala.concurrent.duration._

  implicit val system: ActorSystem = ActorSystem("Transfer")

  implicit val tm: Timeout = 10 seconds
  implicit val ec: ExecutionContext = system.dispatcher
  implicit val mat: ActorMaterializer = ActorMaterializer()

  val router = new ApplicationRouter()

  val bindFuture: Future[Http.ServerBinding] = Http().bindAndHandle(router.rootRoute, http.interface, http.port)
  bindFuture.onComplete {
    case Success(binding) => logger.info(s"Successfully bounded http server: ${binding.localAddress}")
    case Failure(ex)      => logger.warn(s"Unable to bind server: ${ex.getMessage}")
  }

}
