package com.github.agnikowa.moneytransferhandler

import akka.http.scaladsl.server.{Directives, Route}
import com.typesafe.scalalogging.LazyLogging

class ApplicationRouter() extends Directives with LazyLogging {

  val rootRoute: Route = (decodeRequest & encodeResponse) {

    pathEndOrSingleSlash(get(complete("Hello from transfer app")))
  }

}