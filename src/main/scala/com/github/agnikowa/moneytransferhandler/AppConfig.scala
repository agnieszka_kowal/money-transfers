package com.github.agnikowa.moneytransferhandler

import com.typesafe.config.ConfigFactory

trait AppConfig {

  private val config = ConfigFactory.load()

  object http {
    private val http = config.getConfig("http")
    val interface: String = http.getString("interface")
    val port: Int = http.getInt("port")
  }

}
