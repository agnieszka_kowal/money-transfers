name := "money-transfers"

version := "0.1"

scalaVersion := "2.12.3"

resolvers += Resolver.jcenterRepo

libraryDependencies ++= {
  val akkaHttpVer = "10.0.5"
  val akkaV = "2.4.17"
  val configV = "1.3.1"
  val scalaTestV = "3.0.1"
  val logbackV = "1.1.5"
  Seq(
    "com.typesafe.akka"           %% "akka-actor"                   % akkaV,
    "com.typesafe"                 % "config"                       % configV,
    "com.typesafe.akka"           %% "akka-http-core"               % akkaHttpVer,
    "com.typesafe.akka"           %% "akka-http"                    % akkaHttpVer,

    "com.typesafe.scala-logging"  %% "scala-logging"                % "3.7.2",

    "com.github.nscala-time"      %% "nscala-time"                  % "2.16.0",
    "com.typesafe.akka"           %% "akka-http-spray-json"         % akkaHttpVer,

    "joda-time"                    % "joda-time"                    % "2.8.1",

    "com.typesafe.akka"           %% "akka-slf4j"                   % akkaV,
    "ch.qos.logback"               % "logback-classic"              % logbackV,

    "org.scalatest"               %% "scalatest"                    % scalaTestV            % Test,
    "com.typesafe.akka"           %% "akka-testkit"                 % akkaV                 % Test,
    "com.typesafe.akka"           %% "akka-http-testkit"            % akkaHttpVer           % Test,
    "com.github.tomakehurst"       % "wiremock"                     % "1.33"                % Test

  )
}